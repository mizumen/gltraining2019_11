﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using News.Models;

namespace News.Services
{
    public static class NewsStorage
    {
        public static List<NewsEntry> News = new List<NewsEntry>
            {
                new NewsEntry
                {
                    Title = "CARYN W. KOEPP LOVES CATS SO MUCH, ADOPTS 30 CATS",
                    Text =
                        @"A local hero came out of the crowd today at the Humane Animal Shelter Society of Washington, DC, where local resident Caryn W. Koepp adopted 30 cats of various breeds and ages during a charity event.
                                Adopting the cats was absolutely free, but taking care of the cats will be a major endeavour.
                                A local bystander claims that Koepp could not decide on which cat to take home, and eventually decided to just take them all."
                },
                new NewsEntry
                {
                    Title = "KENNETH HARTLEY GAINS 400 LBS ON ALL HOT-DOG DIET",
                    Text =
                        @"Kenneth Hartley just cut the mustard, setting a new world record for gaining 400 lbs in less than 8 weeks. Hartley gained an incredible 403 lbs in just 8 weeks, becoming the first person to ever gain over 400 lbs in that timeframe.
                                The 550lb Hartley set the record on an all dog diet, according to Kenneth."
                },
                new NewsEntry
                {
                    Title = "EDWARD EUGENE WANTED FOR TAX EVASION",
                    Text =
                        @"Edward Eugene is wanted by the IRS for owing over 75,000 in back taxes. According to the IRS web site, Eugene has not filed a federal tax return in five years.
                                The penalty for misdemeanor tax evasion is one year in prison for each year evaded, with a maximum of three years imprisonment. Eugene currently faces a three year sentence in a federal district court. He is also being charged with more serious felony charges of tax fraud and conspiracy."
                }
            };
    }
}