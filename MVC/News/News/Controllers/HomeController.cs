﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using News.Models;
using News.Services;

namespace News.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new NewsModel
            {
                News = GetNews()
            };
            return View(model);
        }

        private List<NewsEntry> GetNews()
        {
            return NewsStorage.News;
        }
    }
}