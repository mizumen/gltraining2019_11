﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using GLTraining.WCF.ForumAPI.Models;

namespace GLTraining.WCF.ForumAPI.ConsoleClientRest
{
    class Program
    {
        static void Main(string[] args)
        {
            //DirectReferenceDemo();
            GenericSolutionDemo().Wait();
        }

        private static void DirectReferenceDemo()
        {
            var uri = new Uri("http://localhost:62791/UserService.svc");
            var binding = new WebHttpBinding();
            var channelFactory = new WebChannelFactory<IUserService>(binding, uri);
            
            var proxy = channelFactory.CreateChannel();
            
            var users = proxy.Get();
            proxy.Create(new User
            {
                Id = 42,
                Name = "John"
            });
            
            proxy.Promote("42");
            users = proxy.Get();
            proxy.Demote("42");
            users = proxy.Get();
        }

        private static async Task GenericSolutionDemo()
        {
            var httpClient = new HttpClient() { BaseAddress = new Uri("http://localhost:62791/UserService.svc/") };

            // http://localhost:62791/UserService.svc/users
            var users = await httpClient.GetAsJsonAsync<LocalUser[]>("users");
            
            foreach (var user in users)
            {
                foreach (var action in user.Actions)
                {
                    await httpClient.PutAsJsonAsync(action.Url, user.Id);
                }
            }

            var updatedUsers = await httpClient.GetAsJsonAsync<LocalUser[]>("users");
        }

        






        public class LocalUser
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public bool IsAdmin { get; set; }

            public List<LocalAction> Actions { get; set; } = new List<LocalAction>();
        }

        public class LocalAction
        {
            public string Name { get; set; }
            public string Url { get; set; }
        }
    }

    public static class HttpClientExtensions
    {
        public static async Task<T> GetAsJsonAsync<T>(this HttpClient client, string url)
        {
            var formatters = new[] { new JsonMediaTypeFormatter() };
            var response = await client.GetAsync(url);
            return await response.Content.ReadAsAsync<T>(formatters);
        }
    }
}
