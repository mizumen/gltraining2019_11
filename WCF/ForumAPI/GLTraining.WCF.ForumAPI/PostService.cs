﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Threading;
using GLTraining.WCF.ForumAPI.Models;

namespace GLTraining.WCF.ForumAPI
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PostService : IPostService
    {
        private List<Post> posts = new List<Post>(); 

        public List<Post> GetPosts()
        {
            return posts;
        }
        
        public void AddPost(Post post)
        {
            this.posts.Add(post);
        }

        public List<Post> GetPosts(DateRange range)
        {
            return GetPosts()
               .Where(p => p.DtCreated >= range.From && p.DtCreated < range.To)
               .ToList();
        }

    }
}
