﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using GLTraining.WCF.ForumAPI.Models;
using Action = GLTraining.WCF.ForumAPI.Models.Action;

namespace GLTraining.WCF.ForumAPI
{
    public class UserService : IUserService
    {
        private static readonly List<User> UserStore = new List<User>
        {
            new User { Id = 1, Name = "Alice" },
            new User { Id = 2, Name = "Bob", IsAdmin = true}
        };

        public User[] Get()
        {
            return UserStore.Select(FillLinks).ToArray();
        }

        public User GetById(string id)
        {
            return FillLinks(GetUser(id));
        }

        public void Create(User user)
        {
            UserStore.Add(user);
        }

        public User Update(User user)
        {
            var existingUser = UserStore.First(i => i.Id == user.Id);
            existingUser.Name = user.Name;
            existingUser.IsAdmin = user.IsAdmin;
            return FillLinks(existingUser);
        }

        public void Promote(string id)
        {
            var existingUser = GetUser(id);
            existingUser.IsAdmin = true;
        }

        public void Demote(string id)
        {
            var existingUser = GetUser(id);
            existingUser.IsAdmin = false;
        }

        private User FillLinks(User user)
        {
            var baseUri = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.OriginalString.TrimEnd('/');
            user.Actions = new List<Action>();
            user.Actions.Add(user.IsAdmin
                ? new Action {Name = "demote", Url = $"{baseUri}/users/{user.Id}/demote"}
                : new Action {Name = "promote", Url = $"{baseUri}/users/{user.Id}/promote"});
            return user;
        }

        private static User GetUser(string id)
        {
            var realId = int.Parse(id);
            var existingUser = UserStore.First(i => i.Id == realId);
            return existingUser;
        }
    }
}
