﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GLTraining.WCF.ForumAPI.Models
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsAdmin { get; set; }
        [DataMember]
        public List<Action> Actions { get; set; } = new List<Action>();
    }
}
