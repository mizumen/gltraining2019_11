﻿using System;
using System.Runtime.Serialization;

namespace GLTraining.WCF.ForumAPI.Models
{
    [DataContract]
    public class DateRange
    {
        [DataMember]
        public DateTime From { get; set; }
        [DataMember]
        public DateTime To { get; set; }
    }
}
