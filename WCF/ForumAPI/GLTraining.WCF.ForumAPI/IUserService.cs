﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using GLTraining.WCF.ForumAPI.Models;

namespace GLTraining.WCF.ForumAPI
{
    [ServiceContract]
    public interface IUserService
    {
        [WebGet(UriTemplate = "users", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        User[] Get();

        [WebGet(UriTemplate = "users/{id}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        User GetById(string id);

        [WebInvoke(Method = "POST", UriTemplate = "users", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        void Create(User user);

        [WebInvoke(Method = "PUT", UriTemplate = "users", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        User Update(User user);

        [WebInvoke(Method = "PUT", UriTemplate = "users/{id}/promote", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        void Promote(string id);

        [WebInvoke(Method = "PUT", UriTemplate = "users/{id}/demote", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        void Demote(string id);
    }
}
