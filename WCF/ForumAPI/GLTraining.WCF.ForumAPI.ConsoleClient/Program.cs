﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLTraining.WCF.ForumAPI.ConsoleClient.PostService.SelfHost;

namespace GLTraining.WCF.ForumAPI.ConsoleClient
{
    class Program
    {
        private static int _idCounter = 1;
        static void Main(string[] args)
        {
            DisplayPosts();
            AddPost("Alice", "My first post");
            AddPost("Bob", "Yet another post");
            DisplayPosts();

            Console.ReadKey();
        }

        private static void AddPost(string author, string postBody)
        {
            using (var proxy = new PostService.SelfHost.PostServiceClient("NetTcpBinding_IPostService"))
            {
                proxy.AddPost(new Post { Author = author, DtCreated = DateTime.UtcNow, Id = _idCounter, Text = postBody });
                _idCounter++;
            }
        }

        private static void DisplayPosts()
        {
            Console.WriteLine("-----------");
            Console.WriteLine("Displaying posts...");
            using (var proxy = new PostService.SelfHost.PostServiceClient("BasicHttpBinding_IPostService1"))
            {
                var posts = proxy.GetPosts();
                foreach (var post in posts)
                {
                    Console.WriteLine($"[{post.DtCreated}] {post.Author} {post.Text}");
                }
            }

            Console.WriteLine("-----------");
        }
    }
}
