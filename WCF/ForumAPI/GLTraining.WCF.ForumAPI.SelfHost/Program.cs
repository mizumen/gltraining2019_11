﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace GLTraining.WCF.ForumAPI.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(typeof(PostService));

            try
            {
                host.Open();
                Console.WriteLine("Service started at " + string.Join(",", host.BaseAddresses));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
            host.Close();
        }
    }
}
