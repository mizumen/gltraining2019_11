﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.API.Models
{
    [DataContract]
    public class MatchInfo
    {
        [DataMember]
        public DateTime DtPlayed { get; set; }
        [DataMember]
        public string Winner { get; set; }
    }
}
