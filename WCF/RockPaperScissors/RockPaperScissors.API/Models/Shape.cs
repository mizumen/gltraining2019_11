﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.API.Models
{
    [DataContract]
    public enum Shape : byte
    {
        Unknown,
        [EnumMember]
        Rock,
        [EnumMember]
        Paper,
        [EnumMember]
        Scissors
    }
}
