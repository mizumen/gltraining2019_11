﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using RockPaperScissors.API.Models;

namespace RockPaperScissors.API
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class MatchService : IMatchService
    {
        private readonly List<MatchInfo> _inMemoryStore = new List<MatchInfo>();
        private readonly Random _random = new Random();

        public void AddMatchInfo(MatchInfo model)
        {
            this._inMemoryStore.Add(model);
        }

        public IEnumerable<MatchInfo> GetHistory()
        {
            return this._inMemoryStore;
        }

        public Shape GetShape()
        {
            var values = Enum.GetValues(typeof (Shape)).Cast<Shape>().ToArray();
            var randomItem = values[_random.Next(1, values.Length)];

            return randomItem;
        }
    }
}