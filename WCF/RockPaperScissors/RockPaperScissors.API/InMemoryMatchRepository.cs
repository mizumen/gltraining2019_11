﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.API.Models;

namespace RockPaperScissors.API
{
    public interface IMatchRepository
    {
        void Save(MatchInfo m);
        IEnumerable<MatchInfo> Get();
    }

    public class InMemoryMatchRepository : IMatchRepository
    {
        private readonly List<MatchInfo> _store = new List<MatchInfo>(); 

        public IEnumerable<MatchInfo> Get()
        {
            return _store.ToList();
        }

        public void Save(MatchInfo m)
        {
            if (m == null)
                throw new ArgumentNullException(nameof(m));

            _store.Add(m);
        }
    }
}
