﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.API.Models;

namespace RockPaperScissors.API
{
    [ServiceContract]
    public interface IMatchService
    {
        [OperationContract]
        Shape GetShape();

        [OperationContract]
        void AddMatchInfo(MatchInfo model);

        [OperationContract]
        IEnumerable<MatchInfo> GetHistory();
    }
}
