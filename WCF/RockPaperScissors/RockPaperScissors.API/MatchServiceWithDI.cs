﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.API.Models;

namespace RockPaperScissors.API
{
    public class MatchServiceWithDI : IMatchService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly Random _random = new Random();

        public MatchServiceWithDI(IMatchRepository matchRepository)
        {
            _matchRepository = matchRepository;
        }

        public Shape GetShape()
        {
            var values = Enum.GetValues(typeof(Shape)).Cast<Shape>().ToArray();
            var randomItem = values[_random.Next(1, values.Length)];

            return randomItem;
        }

        public void AddMatchInfo(MatchInfo model)
        {
            this._matchRepository.Save(model);
        }

        public IEnumerable<MatchInfo> GetHistory()
        {
            return this._matchRepository.Get();
        }
    }
}
