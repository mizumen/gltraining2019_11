﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.MatchMaker.Contracts;
using RockPaperScissors.MatchMaker.Models;
// ReSharper disable all

namespace RockPaperScissors.MatchMaker
{
    public class WinnerOracle : IWinnerOracle
    {
        public IEnumerable<PlayerInfo> FindWinner(IEnumerable<PlayerInfo> players)
        {
            var distinctShapes = players.Select(s => s.Shape).Distinct().ToList();
            if (distinctShapes.Count == 1)
                return players; // everybody is a winner
            if (distinctShapes.Count != 2)
                return Enumerable.Empty<PlayerInfo>(); // draw

            var winnerShape = CompareShapes(distinctShapes[0], distinctShapes[1]);
            return players.Where(p => p.Shape == winnerShape).ToList();
        }

        // ...

        private static Shape CompareShapes(Shape first, Shape second)
        {
            var isFirstWinner = false;
            switch (first)
            {
                case Shape.Paper:
                    isFirstWinner = second == Shape.Rock;
                    break;
                case Shape.Rock:
                    isFirstWinner = second == Shape.Scissors;
                    break;
                case Shape.Scissors:
                    isFirstWinner = second == Shape.Paper;
                    break;
            }

            return isFirstWinner ? first : second;
        }
    }
}
