﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.MatchMaker.Contracts;
using RockPaperScissors.MatchMaker.Models;

namespace RockPaperScissors.MatchMaker
{
    public class MatchServiceProxy : IMatchInfoRepository, IShapeSelector
    {
        public Shape GetShape()
        {
            using (var client = CreateClient())
            {
                return (Shape) client.GetShape();
            }
        }

        public void AddMatchInfo(MatchInfo model)
        {
            using (var client = CreateClient())
            {
                client.AddMatchInfo(new MatchServiceWithDI.MatchInfo
                {
                    DtPlayed = model.DtPlayed,
                    Winner = model.Winner
                });
            }
        }

        // ...

        public IEnumerable<MatchInfo> GetHistory()
        {
            using (var client = CreateClient())
            {
                var dtoHistory = client.GetHistory();
                return dtoHistory.Select(h => new MatchInfo
                {
                    DtPlayed = h.DtPlayed,
                    Winner = h.Winner
                }).ToList();
            }
        }

        private MatchServiceWithDI.MatchServiceClient CreateClient() 
            => new MatchServiceWithDI.MatchServiceClient("BasicHttpBinding_IMatchService1");
    }
}
