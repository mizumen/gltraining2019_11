﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.MatchMaker.Models;

namespace RockPaperScissors.MatchMaker.Contracts
{
    public interface IWinnerOracle
    {
        IEnumerable<PlayerInfo> FindWinner(IEnumerable<PlayerInfo> players);
    }
}
