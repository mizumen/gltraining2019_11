﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.MatchMaker.Models;

namespace RockPaperScissors.MatchMaker.Contracts
{
    public interface IMatchInfoRepository
    {
        void AddMatchInfo(MatchInfo model);

        IEnumerable<MatchInfo> GetHistory();
    }

    public interface IShapeSelector
    {
        Shape GetShape();
    }
}
