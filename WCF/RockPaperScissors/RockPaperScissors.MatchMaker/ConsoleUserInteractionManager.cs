﻿using System;
using System.Linq;
using RockPaperScissors.MatchMaker.Contracts;

namespace RockPaperScissors.MatchMaker
{
    public class ConsoleUserInteractionManager : IUserInteractionManager
    {
        private readonly IMatchInfoRepository _matchHistoryRepository;

        public ConsoleUserInteractionManager(
            IMatchInfoRepository matchHistoryRepository)
        {
            _matchHistoryRepository = matchHistoryRepository;
        }

        // ...

        public bool ShouldStartGame()
        {
            Console.WriteLine("Press [Enter] for game start or any key to exit");
            var key = Console.ReadKey();
            if (key.Key == ConsoleKey.Enter)
            {
                Console.WriteLine("Let's play a game!");
                return true;
            }

            return false;
        }

        public void OnFinish()
        {
            Console.WriteLine("------------");
            Console.WriteLine("------------");
            Console.WriteLine("Displaying statistics...");

            var history = _matchHistoryRepository.GetHistory();
            foreach (var group in history.GroupBy(s => s.Winner).OrderByDescending(g => g.Count()))
            {
                Console.WriteLine($"{group.Key}: {group.Count()} win count!");
            }
        }
    }
}