﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.MatchMaker.Contracts;
using RockPaperScissors.MatchMaker.Models;

namespace RockPaperScissors.MatchMaker
{
    public class GameProcessor : IGameProcessor
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IMatchInfoRepository _matchInfoRepository;
        private readonly IWinnerOracle _winnerOracle;
        private readonly IUserInteractionManager _userInteractionManager;

        public GameProcessor(IPlayerRepository playerRepository, 
            IMatchInfoRepository matchInfoRepository,
            IWinnerOracle winnerOracle,
            IUserInteractionManager userInteractionManager)
        {
            _playerRepository = playerRepository;
            _matchInfoRepository = matchInfoRepository;
            _winnerOracle = winnerOracle;
            _userInteractionManager = userInteractionManager;
        }

        // ...

        public void Process()
        {
            while (_userInteractionManager.ShouldStartGame())
            {
                var players = this._playerRepository.GetPlayerInfos();
                var winners = this._winnerOracle.FindWinner(players);

                foreach (var winner in winners)
                {
                    var matchResult = new MatchInfo
                    {
                        DtPlayed = DateTime.UtcNow,
                        Winner = winner.Name
                    };
                    this._matchInfoRepository.AddMatchInfo(matchResult);
                }
            }

            _userInteractionManager.OnFinish();
        }
    }
}
