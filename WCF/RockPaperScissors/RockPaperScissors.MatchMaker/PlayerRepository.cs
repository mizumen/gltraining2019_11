﻿using System.Collections.Generic;
using RockPaperScissors.MatchMaker.Contracts;
using RockPaperScissors.MatchMaker.Models;

namespace RockPaperScissors.MatchMaker
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly IShapeSelector _shapeSelector;
        private static readonly string[] PlayerNames = {"Alice", "Bob"};

        public PlayerRepository(IShapeSelector shapeSelector)
        {
            _shapeSelector = shapeSelector;
        }

        public IEnumerable<PlayerInfo> GetPlayerInfos()
        {
            var players = new List<PlayerInfo>();
            foreach (var playerName in PlayerNames)
            {
                var player = new PlayerInfo
                {
                    Name = playerName,
                    Shape = _shapeSelector.GetShape()
                };
                players.Add(player);
            }

            return players;
        }
    }
}
