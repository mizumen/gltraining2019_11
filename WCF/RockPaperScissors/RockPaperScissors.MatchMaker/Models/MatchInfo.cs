﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.MatchMaker.Models
{
    public class MatchInfo
    {
        public DateTime DtPlayed { get; set; }
        public string Winner { get; set; }
    }
}
