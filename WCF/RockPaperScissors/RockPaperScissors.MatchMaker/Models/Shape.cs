﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.MatchMaker.Models
{
    public enum Shape : byte
    {
        Unknown,
        Rock,
        Paper,
        Scissors
    }
}
