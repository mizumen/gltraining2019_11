﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors.MatchMaker.Models
{
    public class PlayerInfo
    {
        public string Name { get; set; }
        public Shape Shape { get; set; }
    }
}
