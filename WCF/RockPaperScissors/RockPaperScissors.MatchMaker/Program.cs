﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using RockPaperScissors.MatchMaker.Contracts;

namespace RockPaperScissors.MatchMaker
{
    class Program
    {
        [Obsolete("Manual way of dependency management")]
        static void Main123(string[] args)
        {
            var matchServiceProxy = new MatchServiceProxy();
            var playerRepository = new PlayerRepository(matchServiceProxy);
            var userInteractionManager = new ConsoleUserInteractionManager(matchServiceProxy);
            var gameOracle = new WinnerOracle();

            var processor = new GameProcessor(playerRepository, 
                matchServiceProxy, 
                gameOracle, 
                userInteractionManager);
            processor.Process();

            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            var container = BuildContainer();
            var gameProcessor = container.Resolve<IGameProcessor>();

            gameProcessor.Process();

            Console.ReadKey();
        }

        private static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ConsoleUserInteractionManager>().As<IUserInteractionManager>();
            builder.RegisterType<MatchServiceProxy>()
                .As<IMatchInfoRepository>()
                .As<IShapeSelector>()
                .SingleInstance();
            builder.RegisterType<PlayerRepository>().As<IPlayerRepository>().InstancePerDependency();
            builder.Register(c => new WinnerOracle()).As<IWinnerOracle>().SingleInstance();
            builder.RegisterType<GameProcessor>().AsImplementedInterfaces().SingleInstance();

            return builder.Build();
        }
    }
}
